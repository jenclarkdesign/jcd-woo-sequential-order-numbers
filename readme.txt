=== WooCommerce Sequential Order Numbers Pro ===
Author: skyverge
Tags: woocommerce
Requires at least: 4.4
Tested up to: 5.1.1
Requires PHP: 5.4

== Installation ==

1. Upload the entire 'woocommerce-sequential-order-numbers-pro' folder to the '/wp-content/plugins/' directory
2. Activate the plugin through the 'Plugins' menu in WordPress
